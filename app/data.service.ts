import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs/Observable';
import { Observable, BehaviorSubject } from 'rxjs/Rx';

@Injectable()
export class DataService {
  keys : number[] = [70,30,60,20,90];
  private data :BehaviorSubject<number[]> = new BehaviorSubject(this.keys);

  constructor() {};

  public setKey(i:number, val:number) :void {
    this.keys[i]=val;
    this.data.next([...this.keys]);
  }
  public getData() :Observable<number[]> {
    return this.data.asObservable();
  }
  public getKeys() :number[] {
    return this.keys;
  }
}
