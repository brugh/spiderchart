"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var primeng_1 = require('primeng/primeng');
var data_service_1 = require('./data.service');
var SlidersComponent = (function () {
    function SlidersComponent(dataService) {
        this.dataService = dataService;
        this.keys = dataService.getKeys();
    }
    ;
    SlidersComponent.prototype.change = function (i, val) {
        this.dataService.setKey(i, val);
    };
    SlidersComponent = __decorate([
        core_1.Component({
            selector: 'sliders',
            template: "\n      <h2>Sliders</h2>\n      <p>{{keys[0]}}\n        <p-slider [(ngModel)]=\"keys[0]\" (onChange)=\"change(0,keys[0])\" [min]=\"0\" [max]=\"100\"></p-slider>\n      </p>\n      <p>{{keys[1]}}\n        <p-slider [(ngModel)]=\"keys[1]\" (onChange)=\"change(1,keys[1])\" [min]=\"0\" [max]=\"100\"></p-slider>\n      </p>\n      <p>{{keys[2]}}\n        <p-slider [(ngModel)]=\"keys[2]\" (onChange)=\"change(2,keys[2])\" [min]=\"0\" [max]=\"100\"></p-slider>\n      </p>\n      <p>{{keys[3]}}\n        <p-slider [(ngModel)]=\"keys[3]\" (onChange)=\"change(3,keys[3])\" [min]=\"0\" [max]=\"100\"></p-slider>\n      </p>\n      <p>{{keys[4]}}\n        <p-slider [(ngModel)]=\"keys[4]\" (onChange)=\"change(4,keys[4])\" [min]=\"0\" [max]=\"100\"></p-slider>\n      </p>\n    ",
            directives: [primeng_1.Slider],
            providers: []
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService])
    ], SlidersComponent);
    return SlidersComponent;
}());
exports.SlidersComponent = SlidersComponent;
//# sourceMappingURL=sliders.component.js.map