import { NgModule,enableProdMode }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }  from './app.component';
import { DataService }   from './data.service';
enableProdMode();

@NgModule({
  imports: [ BrowserModule, FormsModule ],
  declarations: [ AppComponent ],
  providers: [ DataService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
