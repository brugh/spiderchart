import { Component } from '@angular/core';
import { SlidersComponent } from "./sliders.component";
import { GraphComponent } from "./graph.component";

@Component({
    selector: 'my-app',
    template: `
      <div class="content">
        <div class="is-info">
          <h1>Sliders move graphs</h1>
        </div>
        <div class="columns">
          <div class="column is-one-quarter">
            <sliders></sliders>
          </div><div class="column">
            <canvas graph id="myChart" height="250" width="350"></canvas>
          </div>
        </div>
      </div>
    `,
    directives: [SlidersComponent,GraphComponent]
})
export class AppComponent {
  constructor() {}
}
