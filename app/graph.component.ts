import { Directive, ElementRef, Renderer } from '@angular/core';
//import { UIChart } from 'primeng/primeng';
import { DataService } from './data.service';

@Directive({
  selector: '[graph]'
})

export class GraphComponent {
  ctx: any;
  Chart: any;

  dataset:any = {
    label: 'MOQIT data',
    data: [50,30,70,10,90],
    backgroundColor: 'rgba(179,81,98,0.2)',
    borderColor: 'rgba(179,81,98,1)',
    pointBackgroundColor: 'rgba(179,81,98,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(179,81,98,1)'
  };
  data: any = {
    labels: ['Money', 'Organisation', 'Quality', 'Information', 'Time'],
    datasets: [this.dataset]
  };
  options: any = {
    scale : {
      ticks: {
        beginAtZero: true,
        stepSize: 20,
        max: 100
      }
    }
  };

  constructor(private dataService: DataService, el:ElementRef, ren:Renderer) {
  var Chart = require('chart.js/dist/Chart');
    this.ctx = el.nativeElement.getContext("2d");
    this.Chart = new Chart(this.ctx, {
      type:"radar",
      data:this.data,
      options:this.options
    });

    dataService.getData().subscribe(data => {
      this.data.datasets[0].data = data;
      this.Chart.update();
    });
  };
}
