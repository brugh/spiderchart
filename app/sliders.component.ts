import { Component } from '@angular/core';
import { Slider } from 'primeng/primeng';
import { DataService } from './data.service';

@Component({
    selector: 'sliders',
    template: `
      <h2>Sliders</h2>
      <p>{{keys[0]}}
        <p-slider [(ngModel)]="keys[0]" (onChange)="change(0,keys[0])" [min]="0" [max]="100"></p-slider>
      </p>
      <p>{{keys[1]}}
        <p-slider [(ngModel)]="keys[1]" (onChange)="change(1,keys[1])" [min]="0" [max]="100"></p-slider>
      </p>
      <p>{{keys[2]}}
        <p-slider [(ngModel)]="keys[2]" (onChange)="change(2,keys[2])" [min]="0" [max]="100"></p-slider>
      </p>
      <p>{{keys[3]}}
        <p-slider [(ngModel)]="keys[3]" (onChange)="change(3,keys[3])" [min]="0" [max]="100"></p-slider>
      </p>
      <p>{{keys[4]}}
        <p-slider [(ngModel)]="keys[4]" (onChange)="change(4,keys[4])" [min]="0" [max]="100"></p-slider>
      </p>
    `,
    directives: [Slider],
    providers: []
})
export class SlidersComponent {
  keys: number[];
  constructor(private dataService: DataService) {
    this.keys = dataService.getKeys();
  };
  change(i:number, val:number) :void {
    this.dataService.setKey(i,val);
  }
}
