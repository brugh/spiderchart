"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
//import { UIChart } from 'primeng/primeng';
var data_service_1 = require('./data.service');
var GraphComponent = (function () {
    function GraphComponent(dataService, el, ren) {
        var _this = this;
        this.dataService = dataService;
        this.dataset = {
            label: 'MOQIT data',
            data: [50, 30, 70, 10, 90],
            backgroundColor: 'rgba(179,81,98,0.2)',
            borderColor: 'rgba(179,81,98,1)',
            pointBackgroundColor: 'rgba(179,81,98,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(179,81,98,1)'
        };
        this.data = {
            labels: ['Money', 'Organisation', 'Quality', 'Information', 'Time'],
            datasets: [this.dataset]
        };
        this.options = {
            scale: {
                ticks: {
                    beginAtZero: true,
                    stepSize: 20,
                    max: 100
                }
            }
        };
        var Chart = require('chart.js/dist/Chart');
        this.ctx = el.nativeElement.getContext("2d");
        this.Chart = new Chart(this.ctx, {
            type: "radar",
            data: this.data,
            options: this.options
        });
        dataService.getData().subscribe(function (data) {
            _this.data.datasets[0].data = data;
            _this.Chart.update();
        });
    }
    ;
    GraphComponent = __decorate([
        core_1.Directive({
            selector: '[graph]'
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, core_1.ElementRef, core_1.Renderer])
    ], GraphComponent);
    return GraphComponent;
}());
exports.GraphComponent = GraphComponent;
//# sourceMappingURL=graph.component.js.map